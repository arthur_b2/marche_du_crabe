"""

Main functions of the Crab Walk Project

"""

import random
from pprint import pprint
import os
import pygame
import pygame_textinput

def init_game():
    """
    Initialisation du jeu

    Actions : 
    - mélanger les pioches des vilains (rouges et bleus séparément)
    - générer le plateau en répartissant les objets sur la plage
    - tirer au hasard les 4 objets qui seront les pièges
    - d'initialiser le compteur de points de vie à 5


    Entrée : Aucune 

    Sortie : 
    - liste str (6x6) = plateau de départ
    - liste de tuples = pioche vilains rouges
    - liste de tuples = pioche vilains bleus
    - entier valant 5 (vies)
    - liste de 2 tuples représentant les 2 pièges connus du crabe rouge
    - liste de 2 tuples représentant les 2 pièges connus du crabe bleu
    """
    
    # Création du plateau de jeu
    BOARD = [["   "] * 6 for _ in range(6)]
    
    # Appel des objets
    OBJ_LIST = [
    ( " PS" ,0) ,( " ME" ,0) ,( " GC" ,1) ,( " BE" ,1) ,( " PN" ,2) ,( " BM" ,2) ,
( " BC" ,3) ,( " LG" ,3) ,( " BV" ,4) ,( " CT" ,4) ,( " BL" ,5) ,( " SP" ,5)
]
    
    num_ligne=[0,1,2,3,4,5]
    num_col = [0,1,2,3,4,5,0,1,2,3,4,5] #Permet d'éviter d'avoir plus de deux objets par colonne et ligne avec var du dessus.

    obj_list_new_board=[]

    for _ in range(6): 
        #Sélection des objets de la ligne
        tirage_ligne = random.choice(num_ligne)
        resultats = [obj for obj in OBJ_LIST if obj[1] == tirage_ligne]
        num_ligne.remove(tirage_ligne) 

        #Détermination des colonnes pour les pairs d'objets
        tirage_colonne = random.choice(num_col)
        num_col.remove(tirage_colonne)


        # Stockage dans une liste pour le premier item
        obj_temporary = list(resultats[0]) # " ME" --> nom de l'obj
        obj_temporary.append(tirage_colonne) # [" ME",2] --> ajouter la colonne 
        obj_list_new_board.append(obj_temporary) # ajoute [" ME",2] dans obj_list_new_board
            
        # Rebelote pour le deuxième item
        
        # Quand il reste 3 chiffres dans tirage_colonne et que 2 chiffres sont similaire prendre un des deux 
        if len(num_col) == 3 and len(set(num_col)) != 3:
            tirage_colonne2 = random.choice([x for x in num_col if num_col.count(x) > 1])
        else: # Sinon tire aléatoirement et regarder si tirage 1 != 2    
            tirage_colonne2 = random.choice(num_col)
            while tirage_colonne2 == tirage_colonne:
                tirage_colonne2 = random.choice(num_col)

        num_col.remove(tirage_colonne2) # Enlever la colonne 

        obj_temporary = list(resultats[1])
        obj_temporary.append(tirage_colonne2)
        obj_list_new_board.append(obj_temporary)

    # Avec la nouvelle composée comme suit : ("nom_objet, ligne, col"), on ajoute ensuite les objets dans le tableau.
    for item in obj_list_new_board:
        BOARD[item[1]][item[2]] = item[0]
    
    #Placer le crabeux ------------------
    if BOARD[5][0] == "   ":
        BOARD[5][0] = "X  "
    elif BOARD[5][1] == "   ":
        BOARD[5][1] = "X  "
    else:
        BOARD[5][2] = "X  "


    # Liste de tuples représentant la pioche des vilains rouges ------------------
    VIL_RED = [( " TR" ,0) ,( " TR" ,0) ,( " TR" ,1) ,( " HR" ,1) ,( " TR" ,2) ,( " TR" ,2) ,
    ( " TR" ,3) ,( " HR" ,3) ,( " TR" ,4) ,( " TR" ,4) ,( " TR" ,5) ,( " HR" ,5) ]


    # Liste de tuples représetant la pioche des vilains bleues ------------------
    VIL_BLU = [( " TB" ,0) ,( " HB" ,0) ,( " TB" ,1) ,( " TB" ,1) ,( " TB" ,2) ,( " HB" ,2) ,
    ( " TB" ,3) ,( " TB" ,3) ,( " TB" ,4) ,( " HB" ,4) ,( " TB" ,5) ,( " TB" ,5) ]

    # Entier valant 5 (vos crevettes) -------------------------
    LIFE = 5

    # Mélange des pioches vilains-----------------------------------------
    random.shuffle(VIL_RED)
    random.shuffle(VIL_BLU)

    #Définir 4 pièges du début ------------------
    BEG_TRAP = random.sample(OBJ_LIST, k=4)

    # Une liste de 2 tuples représentant les pièges connus du crabe rouge ------------------
    CRAB_RED_TRAP_KNOWN = random.sample(BEG_TRAP,2)

    # une liste de 2 tuples représentant les pièges connus du crabe bleu.
    CRAB_BLU_TRAP_KNOWN = list(set(BEG_TRAP) ^ set(CRAB_RED_TRAP_KNOWN))

    
    full_game_start = (BOARD, CRAB_RED_TRAP_KNOWN ,CRAB_BLU_TRAP_KNOWN, LIFE, VIL_RED, VIL_BLU)

    return full_game_start


def str_game(board, shrimps):
    """
    Afficher l'état du jeu
    
    Entrée : affichage plateau + vie

    Sortie : chaîne de caractères représentant l'état du jeu afin de pouvoir l'afficher avec print
    """

    #Afficher les numéros de colonne ------------------
    result = "     A     B     C     D     E     F\n"
    result += "  ------------------------------------\n"

    #Afficher tab avec lignes ------------------
    for i, ligne in enumerate(board):
        result += f"{i} |" + " | ".join(ligne) + " |\n"
        result += "  ------------------------------------\n"
    
    # Afficher les vie ------------------
    result += f"Crevettes : {shrimps}"
    
    return result


def find_token(board):
    """
    Trouver le pion
    
    Entrée : plateau 

    Sortie : tuple ligne, colonne du pion
    """
    
    for irow in range(6):
        for icol in range(6):
            if "X" in board[irow][icol]:
                return (irow, icol)


def is_free(board, irow, icol):
    """
    Est ce que la case est libre
    
    Entrée : le plateau + ligne, colonne de la case

    Sortie : Émettre un booléen si la case est disponible
    """

    if irow not in range(0, 6):
        return False
    
    if icol not in range(0, 6):
        return False
    
    if board[irow][icol] == "   " or board[irow][icol] == "X  ":
        return True
    
    else:
        return False


def ask_col(board, color, traps, shrimps, vilain):
    """
    Afficher l'état du jeu + demander au joueur au placer le vilain
    
    Entrée : board, color, traps, shrimps, vilain

    Affichage: plateau + vie + color + piege + ou placer le vilain? 

    Sortie : chiffre de la colonne 
    """

    # # ------------------------BOARD------------------------------------

    print(str_game(board, shrimps))
    
    # ----------------------- COLOR and TRAPS -------------------------

    if color == "blue":
        print("Vous êtes Soleil")
    else: 
        print("Vous êtes Bateau")
    
    print(f"Les pièges sont {traps[0][0].replace(" ", "")} et {traps[1][0].replace(" ", "")}")


    # ---------------------- VILAINS ----------------------------------
    
    print(f"Le vilain apparaît dans la ligne : {vilain[1]}")

    ## -----------------------ASK QUESTION WHERE VILAIN --------------------
    is_free_pos = False  
    pos_token_vilain = vilain[1]  # ligne vilain
    num = 0


    while is_free_pos != True:
        is_free_pos = False
        rep_position = input("Dans quelle colonne souhaitez vous le placer ? ").upper() # Input une lettre 
        valeurs_positions = {"A": 0, "B": 1, "C": 2, "D": 3, "E": 4, "F": 5} # Dictionnaire 
        position = ["A","B","C","D","E","F"]

    
        if rep_position in position : # Si input est bien une lettre en A et F
            num = valeurs_positions[rep_position] # num = le chiffre de la colonne
            
        else: # Sinon r
            num = ""
        
        if is_free(board, pos_token_vilain, num) ==  True: #Si la colonne est libre alors arrete le while
            is_free_pos = True
            
    return num #return le numéro de la colonne


def put_vilain(board, irow, icol, vilain):
    """
    Placer le vilain dans le plateau

    Entrée : plateau de jeu, indice de la ligne et de la colonne et le vilain

    Sortie : plateau de jeu modifié 
    """
    
    # Case avec notre pion
    if find_token(board) == (irow,icol):
        board[irow][icol] = f"X{vilain.replace(' ', '')}"
    # Tout autre case
    else:
        board[irow][icol] = f"{vilain}"


def is_valid_move(board, color, start, end):
    """
    Est-ce qu'un mouvement est valide ?

    Entrée : 
    - plateau de jeu 
    - couleur du crabe
    - start = tuple avec les coordonnées de la case de départ
    - end = tuple avec les coordonnées de la case d'arrivée

    Sortie :
    - True si le mouvement est autorisé ou sinon False.
    """
    
    # Vérifie si chaque tuple a bien 2 valeurs (ligne, colonne), et si les valeurs sont comprises entre 0 et 5 pour end et start.
    if len(end) != 2 or len(start) != 2 or not all(0 <= i <= 5 for i in end) or not all(0 <= i <= 5 for i in start) :
        return False
    
    # Vérifie si rouge se déplace verticalement donc si la ligne seulement change.
    if color == "red":

        #vertical: A --> F, la colonne représentée par (start[1]) ne bouge pas ou numéro de ligne en dehors de 0 à 5 inclus.
        # Comme coordonnées = valeurs numériques, on convertit plus tard lettre à chiffre
        if end[1] == start[1] or int(end[0]) not in range(0,6):
            return True
        else:
            return False
    
    
    # Vérifie pour crabe bleu qui se déplace horizontalement donc la colonne change.
    else:
        #bleu ---> horizontal (de 0 à 5). La ligne représentée par start[0] ne bouge pas et numéro de colonne change au sein de 0 à 5 inclus.
        if end[0] == start[0] or int(end[1]) not in range(0,6):
            return True
        else:
            return False


def ask_play(board, color, traps, shrimps):
    """
    Afficher l'état du jeu + demander au joueur au placer le pion
    
    Entrée : board, color, traps, shrimps, vilain

    Affichage: plateau + vie + color + piege + ou placer le pion? 

    Sortie : tuple row,col 
    """
    # # ------------------------BOARD------------------------------------

    print(str_game(board, shrimps))
    # ----------------------- COLOR and TRAPS -------------------------
    if color == "blue":
        print("Vous êtes Soleil")
        
    else: 
        print("Vous êtes Bateau")
    
    print(f"Les pièges sont {traps[0][0]} et {traps[1][0]}")
            
    ## -----------------------ASK QUESTION WHERE U PLAY --------------------
            
    move_valid = False
        
    while move_valid != True:
        rep_position = input("Sur quelle case souhaitez-vous aller ?").upper() #Input
        valeurs_positions = {"A": 0, "B": 1, "C": 2, "D": 3, "E": 4, "F": 5} # Dico
        position = ["A","B","C","D","E","F"]

        start = find_token(board) #irow,icol du pion

        if len(rep_position) == 2 and rep_position[1] in "012345" and rep_position[0] in position: # Si rep a deux element que le premier est contenu entre 0 et 5 et que le deuxieme est une lettre de A à F
            
            end = (int(rep_position[1]), valeurs_positions[rep_position[0]]) # Alors end = tuple rox,col

            if is_valid_move(board, color, start, end) == True: # Est ce que le mouvant est valide
                return end


traps_transformed_to_enemies = [] # Conserve les coordonées du derniers traps tranformés en vilains

def move(game_state, color, irow, icol):
    """
    Bouger le pion et tout ce qui se suit 

    Etape 1 : trouver le pion
    Etape 2: est ce qu'il bouge
    Etape 3: Bouger le pion (l'enelver de sa case pour le mettre dans la case d'arrver)
    Etape 4: Regarder si vilain sur le chemin 
    Etape 5: Si obj sur le end alors CV ou traps
    Etape 6: Si vilain sur la case START est ce qu'ancien traps ?
    Etape 7: Est ce que vilain rencotrer si oui life lost + 1


    Entrée : Game state, la couleur, et la case d'arriver

    Sortie : Change le plateau 
            et retourne le nombre de vie perdu
    """

    # MISE EN PLACE DES VARIABLES

    global traps_transformed_to_enemies # Permet d'appeler la variable globale

    board = game_state[0]

    life_lost = 0
    vilains_rencontrés = 0
    token = find_token(board) # token = row, col du pion

    #Si case d'arriver = Traps ------------------------
    red_vilains_name = ["XTR", "XHR"] 

    # Variable avec les 4 Traps -----------------
    all_traps = []
    all_traps.extend(game_state[1])
    all_traps.extend(game_state[2])
    
    traps_noms_avec_X = []
    
    for i in range(len(all_traps)): # Ajoute le pion dans les traps, " ME" --> "XME"
        name = all_traps[i][0]
        new_name = name.replace(" ", "X", 1)
        traps_noms_avec_X.append(new_name)
    

######---------------------------------    # COMMANDE

    # Est ce qu'il bouge
    if token[0] == irow and token[1] == icol:
        return life_lost
    
    #Mouvement du pion
    board[token[0]][token[1]] = board[token[0]][token[1]].replace("X", " ") # Enlever le X à la case de départ
    board[irow][icol] = board[irow][icol].replace(" ", "X", 1) # Ajouter le X à la case d'arriver
    
    ######## Entre les cases ---------------------------------
    # Vérifier s'il y a eu au moins une rencontre avec un vilain
    # Regarde que les cases entre start et end (min + 1 pour pas avoir le start)
    
    if color == "red": 
        for i in range(min(token[0], irow)+1, max(token[0], irow)):
                if board[i][icol] in [" TB"," HB","XTB", "XHB", " TR", " HR", "XTR", "XHR"]:
                    vilains_rencontrés += 1

    else: 
        for i in range(min(token[1], icol)+1, max(token[1], icol)): # 
                if board[irow][i] in [" TB"," HB","XTB", "XHB", " TR", " HR", "XTR", "XHR"]:
                    vilains_rencontrés += 1

    ###### END ---------------------------------
    # Si end = traps, vie -1 : transfomer objet en vilain rouge ou CV
                       
    
    if board[irow][icol] not in ["X  "]: # Si la case d'arriver n'est pas vide
        if board[irow][icol] in ["XTR", "XHR", "XTB", "XHB"]: # Si c'est un vilain
            vilains_rencontrés += 1

        else:
            if board[irow][icol] in traps_noms_avec_X: # SI elle correspond a un traps
                life_lost += 1
                board[irow][icol] = random.choice(red_vilains_name) # Normalement tire que les vilain rouge car on utilise les vilain bleu pour savori si la partie est perdu
                traps_transformed_to_enemies.append([irow,icol]) # MEttre la position du traps dans une variable

            else: # Sinon c'est un CV
                board[irow][icol] = "XCV"
    
    ####### START---------------------------------

    # Regarder si il y a un vilain, si oui est ce qu'il correspond à la position traps
                
    if board[token[0]][token[1]] in [" TR", " HR", " TB", " HB"]:
        if [token[0],token[1]] == traps_transformed_to_enemies: # Utiliser la variable pour savoir si le tour d'avant il était un traps
            life_lost +=1
            traps_transformed_to_enemies.remove([token[0],token[1]]) # Puis enlver les coordonées 
        vilains_rencontrés +=1
    
    ####### Si rencontrer un vilain ---------------------------------
    if vilains_rencontrés > 0:
        life_lost +=1
    
    return life_lost
    

def is_game_over(board, shrimps):
    """
    La fonction permet de savoir si le joeur a fini la partie
    
    Entree: le plateau et les vies restantes 
    Sortie : Gagné ou Perdu
    """
    crabe_vert = 0
    nb_blue_vilains = 0

    for irow in range(len(board)): # Pour toutes les lignes        
        crabe_vert += sum(board[irow][icol] == " CV" or board[irow][icol] == "XCV" for icol in range(len(board))) # Faire la somme du nombre de crabe vert 
        if crabe_vert == 8:            
            return "Gagné"
    
    if shrimps <= 0:
        return "Perdu"
    
    for irow in range(len(board)):
        nb_blue_vilains += sum(board[irow][icol] == " TB" or board[irow][icol] == " HB" or board[irow][icol] == "XTB" or board[irow][icol] == "XHB" for icol in range(len(board))) # Calcule le nombre de vilain bleu 
        # print("décompte_vilain",nb_red_vilains)
        if nb_blue_vilains >= 12: # SI le nombre de vilain bleu est 12 cela veut dire que tout les vilains on était posé
            return "Perdu"
        
    return " "
    
class Affichage_Pygame:

    def init_pygame():
        """
        Initialisation de Pygame
        Définition de la fenêtre de jeu, de la taille du plateau et des cases, de la police d'écriture et des fps.

        Entrée : Aucune

        Sortie : dictionnaire contenant taille de la fenêtre, fps, font, taille des cases, centre du plateau
        """

        # Démarrage pygame
        pygame.init()

        # Taille écran
        WIDTH = 1000
        HEIGHT = 900

        # Taille plateau de jeu 4/5 de la fenêtre de jeu
        board_size = [WIDTH // (4/5), HEIGHT // (4/5)]
        
        # Taille d'une case du plateau : valeur 13 arbitraire
        case_size = [board_size[0] // 13, board_size[1] // 13]

        # Milieu de la fenêtre 
        board_middle = [(WIDTH - case_size[0] * 6)//2, (HEIGHT - case_size[1] * 6)//2 ]

        # Crée la fenêtre de jeu
        screen = pygame.display.set_mode([WIDTH, HEIGHT])
        pygame.display.set_caption("Marche du Crabe") # Intitulé de la fenêtre
        fps = 60  # Images par seconde

        font = pygame.font.Font(None, 36) # Police d'écriture à changer

        game_info = {"screen" : screen, "fps" : fps, "font" : font, "case_size" : case_size, "board_middle" : board_middle} # retourne toutes les infos

        return game_info

    def image_loader():
        """
        Génération d'un dictionnaire contenant les chemins d'accès aux images du jeu pour chacune d'entre elles.

        Entrée : Aucune

        Sortie : Dictionnaire avec chaque paire = nom_image : chemin_daccès

        """

        dossier = "img/"
        images = {}

        
        # Boucle pour récupérer les images selon leur nom et attribuer une dimension 80x80
        for fichier in os.listdir(dossier): # parcourt des noms du dossier
            if fichier.endswith(".png"): # prend en compte seulement les images
                chemin = os.path.join(dossier, fichier) # définit le chemin 
                image = pygame.image.load(chemin) # Chargement image par son chemin
                image_redimensionnee = pygame.transform.scale(image, (80, 80)) # redimensionne l'image
                images[fichier] = image_redimensionnee # l'ajoute au dictionnaire avec clé:valeur = fichier : image_redim


        # Augmentation de la taille des images pour les vies
        for key in images:
            if "life" in key:
                image = images[key]
                image_redimensionnee = pygame.transform.scale(image, (100, 100)) 
                images[key] = image_redimensionnee

        return images

    def draw_board_screen(board, game_info, images, shrimps):
        """
        Fonction basée sur le même principe que str_game, juste elle affiche dans la fenêtre graphique

        Entrée : plateau de jeu, infos fenêtre de jeu, images du jeu, vies

        Sortie : Aucune, juste des affichages dans la fenêtre de jeu

        """

        
        # Définition de variables
        LETTERS = list("ABCDEF") 
        font = pygame.font.Font(None, 36)
        letter_spacing = -10
        letter_spacing2 = 10

        board_middle = game_info["board_middle"]
        case_size = game_info["case_size"]
        screen = game_info["screen"]

        # Coordonnées horizontales (ABCDEF)
        for j in range(6):
            position_text = font.render(LETTERS[j], True, (0,0,0)) # Ecriture de la lettre concernée
            text_width = position_text.get_width() # Largeur du texte affiché
            text_height = position_text.get_height() # Hauteur du texte affiché

            # Position des  lettres dans la fenêtre 
            x_position = board_middle[0] + (j * (case_size[0] + letter_spacing)) - text_width //2 # milieu plateau +/- selon la case + espace entre lettre - taille prise par l'affichage //2 pour centrer sur la case.
            y_position = 100 - case_size[1] //2 - text_height - 10 # 100 plus bas que le haut de fenêtre - centre de la case -10 pour décaler vers le haut.      
            screen.blit(position_text, (x_position, y_position)) # Projète dans la fenêtre de jeu

        # Coordonnées verticales (012345)   
        for i in range(6):
            position_text = font.render(str(i), True, (0,0,0)) # Ecriture de la lettre concernée
            text_width = position_text.get_width() # Largeur du texte affiché
            text_height = position_text.get_height() # Hauteur du texte affiché
            x_position = board_middle[0] - case_size[0] // 2 - text_width // 2 - 15 # Décalage de -15 par rapport aux cases de gauche du plateau
            y_position = 100 +  (i * (case_size[1] + letter_spacing2)) - text_height // 2 #100 à partir de la gauche du plateau + prise en compte de la case cette fois-ci pour décaler vers le bas
            screen.blit(position_text, (x_position, y_position)) # Projète dans la fenêtre


        for i in range(6): # col
            for j in range(6): # ligne
                
                x_position = board_middle[0] + (j * case_size[1]) - case_size[0] //2 # Position x des cases : part du milieu +/- selon le numéro de la case et on positionne au centre
                y_position = 100 + (i * case_size[0]) - case_size[1] //2 # 100 décalage du bord gauche de la fenêtre de jeu, selon la case on multiplie par son numéro et on centre
                # - case_size[0] et [1] décale le plateau vers le haut gauche, n'est pas obligatoire.


                # Selon la nature de la case, projète l'image correspondante
                if board[i][j] == "   " or board[i][j] == "X  ": #vide ou avec X => Case plateau vide
                    screen.blit(images[f"{i}_empty.png"], (x_position, y_position))

                # Si certains vilains sont présents
                elif "TB" in board[i][j]: # Vilain TB
                    screen.blit(images[f"{i}_TB_01.png"], (x_position, y_position))
                elif "HB" in board[i][j]: # Vilain HB
                    if i in [1,3,5]: # Ces indices ne présentent pas de HB donc TB à la place
                        screen.blit(images[f"{i}_TB_01.png"], (x_position, y_position))
                    screen.blit(images[f"{i}_HB_01.png"], (x_position, y_position))
                elif "TR" in board[i][j]: # Vilain TR
                    screen.blit(images[f"{i}_TR_01.png"], (x_position, y_position))
                elif "HR" in board[i][j]: # Vilain HR
                    if i in [0,2,4]: # ces indices ne présentent pas de HR donc TR à la place
                        screen.blit(images[f"{i}_TR_01.png"], (x_position, y_position)) 
                    screen.blit(images[f"{i}_HR_01.png"], (x_position, y_position))

                # Présence de crabes verts
                elif "CV" in board[i][j]: 
                    random_number = random.randint(1,2) # Deux images pour CV
                    screen.blit(images[f"{i}_CV_0{random_number}.png"], (x_position, y_position))

                # Présence d'objet
                else: 
                    screen.blit(images[f"{i}_{board[i][j].lstrip()}_01.png"], (x_position, y_position))

        # Positionnement du token à la fin et on le pose au centre de la case
        token_position = find_token(board)
        screen.blit(images["token.png"], (board_middle[0] + (token_position[1] * case_size[1]) - case_size[0] //2, 100 + (token_position[0] * case_size[0]) - case_size[1] //2))

        # Positionnement des vies
        screen.blit(images[f"life_{shrimps}.png"], (board_middle[0] + (5 * case_size[0]) , 100 + (2* case_size[1]))) # board_middle[1] +

    def draw_board_color(color, game_info):
        """
        Fonction graphique pour ajouter la phrase qui définit quel crabe nous sommes 

        Entrée : couleur du crabe, infos de la fenêtre de jeu

        Sortie : Aucune, Affichage fenêtre de jeu

        """

        # Définition des variables

        screen = game_info["screen"]
        board_middle = game_info["board_middle"]
        case_size = game_info["case_size"]

        font = pygame.font.Font(None, 36)
        x_position = board_middle[0]
        y_position = 200 + case_size[1] * 6 + 5

        # Selon la couleur 
        if color == "blue":
            text_blue = font.render("Vous êtes Soleil", True, (0,0,0)) # Ecriture du texte en noir et lissé 
            screen.blit(text_blue, (x_position, y_position)) # Affiche dans la fenêtre
        else: 
            text_red = font.render("Vous êtes Bateau", True, (0,0,0)) # Ecriture du texte en noir et lissé
            screen.blit(text_red, (x_position, y_position)) # Affichage dans la fenêtre

    def draw_board_vilains(vilain, game_info):
        """
        Fonction graphique pour ajouter la phrase évoquant l'apparition du vilain

        Entrée : vilain à ajouter, infos de la fenêtre de jeu

        Sortie : Aucune, Affichage fenêtre de jeu

        """

        # Définition variables 
        screen = game_info["screen"]
        board_middle = game_info["board_middle"]
        case_size = game_info["case_size"]

        font = pygame.font.Font(None, 36)
        x_position = board_middle[0]
        y_position = 250 + case_size[1] * 6 + 5


        text_vilain = font.render(f"Le vilain apparaît dans la ligne : {vilain[1]}", True, (0,0,0)) # Ecriture du texte en noir et lissé
        screen.blit(text_vilain, (x_position, y_position)) # Affichage fenêtre de jeu

    def draw_board_traps(traps, game_info, images):
        """
        Fonction graphique pour l'ajout des trapsdans la fenêtre graphique 

        Entrée : traps, les infos de la fenêtre de jeu et les images

        Sortie : Aucune, Affichage fenêtre de jeu

        """

        # Définition variables
        screen = game_info["screen"]
        board_middle = game_info["board_middle"]
        case_size = game_info["case_size"]


        # Récupère les pièges et on enlève l'espace      
        name_traps_1 = traps[0][0].replace(" ", "")
        name_traps_2 = traps[1][0].replace(" ", "")

        # Récupérer les images des traps dans le dictionnaire images
        for key in images:
            if name_traps_1 in key:
                image_name_1 = key
            if name_traps_2 in key:
                image_name_2 = key
        

        # Affichage image 1 collé à image 2

        position_y = 100 + case_size[1] * 6 + 5 # Position 1 case sous le plateau
        position_x = board_middle[0] + case_size[0] * 3 # Position légèrement sur le bas droite du plateau

        screen.blit(images[image_name_1], (position_x, position_y)) # Affichage du traps 1 sur fenêtre de jeu
        screen.blit(images[image_name_2], (position_x + case_size[0], position_y)) # Affichage du traps 2 sur fenêtre de jeu avec une case de décalage
        
    def ask_col_screen(board, color, traps, vilain, shrimps, game_info, images):
        """
        Fonction graphique pour afficher plateau de jeu et demander au joueur de placer le vilain

        Entrée : 
            - plateau de jeu
            - couleur du crabe
            - pièges selon le joueur
            - vilain qui va être placé
            - vies
            - informations sur la fenêtre de jeu
            - images du jeu

        Sortie : numéro de colonne où le vilain doit être placé

        """

        # # ------------------------BOARD------------------------------------

        Affichage_Pygame.draw_board_screen(board, game_info, images, shrimps)
        
        # ----------------------- COLOR and TRAPS ------------------------- 
        Affichage_Pygame.draw_board_color(color, game_info)
        
        Affichage_Pygame.draw_board_traps(traps, game_info, images)
        
        # # ---------------------- VILAINS ----------------------------------
        
        Affichage_Pygame.draw_board_vilains(vilain, game_info)

        # ## -----------------------ASK QUESTION WHERE VILAIN --------------------

        # Définitions de variables
        screen = game_info["screen"]
        board_middle = game_info["board_middle"]
        case_size = game_info["case_size"]

        font = pygame.font.Font(None, 36) # police d'écriture
        is_free_pos = False  
        pos_token_vilain = vilain[1]  # ligne vilain
        num = 0
        
        x_position_question = board_middle[0] # Position horizontale de la question dans la fenêtre de jeu
        y_position_question = 300 + case_size[1] * 6 + 5 # Position verticale de la question dans la fenêtre de jeu

        text_put_vilain = font.render("Dans quelle colonne souhaitez vous le placer ? ", True, (0,0,0))
        screen.blit(text_put_vilain, (x_position_question, y_position_question)) # Affichage de la question


        x_position_anwser = board_middle[0] + text_put_vilain.get_width() # Position horizontale de la réponse/entrée du joueur à droite de la question
        y_position_anwser = 300 + case_size[1] * 6 + 5 # Position verticale de la réponse/entrée du joueur, 300 pour être en dessous des autres questions


        textinput = pygame_textinput.TextInputVisualizer() # Création de la zone d'entrée du joueur 
        textinput.cursor_width = 0 # Supprime le curseur qui clignote à l'entrée 

        while is_free_pos != True:
            is_free_pos = False

            events = pygame.event.get() # Récupère tous les évènements (mouvements de souris, entrées claviers, clics, etc.)
            textinput.update(events) # Mettre à jour l'état de la variable textinput en fonction des évènements

            # Projète sur la dimension de la fenêtre de réponse du joueur un carré blanc
            screen.fill((255, 255, 255), (x_position_anwser, y_position_anwser, textinput.surface.get_width(), textinput.surface.get_height()))

            # Afficher la zone de texte réponse joueur
            screen.blit(textinput.surface, (x_position_anwser, y_position_anwser))

            pygame.display.update() # Met à jour l'affichage de la fenêtre graphique 

            rep_position = textinput.value # Contient la réponse du joueur


            valeurs_positions = {"A": 0, "B": 1, "C": 2, "D": 3, "E": 4, "F": 5}
            position = ["A","B","C","D","E","F"]

        
            if rep_position in position :
                num = valeurs_positions[rep_position]
                
            else: 
                num = ""
                screen.fill((255, 255, 255), (x_position_anwser, y_position_anwser, textinput.surface.get_width(), textinput.surface.get_height())) # Si l'entrée n'est pas bonne remet une zone blanche dans zone de texte.
            
            if is_free(board, pos_token_vilain, num) ==  True:
                is_free_pos = True

        return num
    
    def ask_play_screen(board, color, traps, shrimps, images, game_info):
        """
        Fonction graphique pour afficher plateau de jeu et demander au joueur de se déplacer

        Entrée : 
            - plateau de jeu
            - couleur du crabe
            - pièges selon le joueur
            - vies
            - images du jeu
            - informations sur la fenêtre de jeu

        Sortie : numéro de colonne où le vilain doit être placé

        """
        
        # ----------------- BOARD ----------------------- 
        Affichage_Pygame.draw_board_screen(board, game_info, images, shrimps)

        # ----------------------- COLOR and TRAPS ------------------------- 
        Affichage_Pygame.draw_board_color(color, game_info)
        
        Affichage_Pygame.draw_board_traps(traps, game_info, images)

        # --------------------- ASk QUESTION WHERE U PLAY ---------------

        # Définitions de variables
        screen = game_info["screen"]
        board_middle = game_info["board_middle"]
        case_size = game_info["case_size"]
        font = pygame.font.Font(None, 36)
        
        x_position_question = board_middle[0] # Position horizontale de la question
        y_position_question = 250 + case_size[1] * 6 + 5 # Position verticale de la question

        move_valid = False
            
        text_put_vilain = font.render("Sur quelle case souhaitez-vous aller ? ", True, (0,0,0)) # Ecriture de la question en noir, lissé
        screen.blit(text_put_vilain, (x_position_question, y_position_question)) # Affiche sur l'écran


        x_position_anwser = board_middle[0] + text_put_vilain.get_width() # Position horizontale de la réponse joueur à droite de la question
        y_position_anwser = 250 + case_size[1] * 6 + 5 # Position verticale de la réponse joueur


        textinput = pygame_textinput.TextInputVisualizer() # Initialisation de la zone de saisie de texte 
        textinput.cursor_width = 0 # Supprime le curseur qui clignote à la saisie de texte

        while move_valid != True:
            
            events = pygame.event.get() # Récupère tous les évènements (mouvements de souris, entrées claviers, clics, etc.)
            textinput.update(events) # Mettre à jour l'état de la variable textinput en fonction des évènements

            # Projète sur la dimension de la fenêtre de réponse du joueur un carré blanc
            screen.fill((255, 255, 255), (x_position_anwser, y_position_anwser, textinput.surface.get_width(), textinput.surface.get_height()))

            # Afficher la zone de texte réponse joueur
            screen.blit(textinput.surface, (x_position_anwser, y_position_anwser))

            pygame.display.update() # Met à jour l'affichage de la fenêtre graphique 

            rep_position = textinput.value # Contient la réponse du joueur
            
            valeurs_positions = {"A": 0, "B": 1, "C": 2, "D": 3, "E": 4, "F": 5}
            position = ["A","B","C","D","E","F"]

            start = find_token(board)

            if len(rep_position) == 2 and rep_position[1] in "012345" and rep_position[0] in position:
                
                end = (int(rep_position[1]), valeurs_positions[rep_position[0]])

                if is_valid_move(board, color, start, end) == True:
                    return end

            screen.fill((255, 255, 255), (x_position_anwser, y_position_anwser, textinput.surface.get_width(), textinput.surface.get_height())) #  Si l'entrée n'est pas bonne remet une zone blanche dans zone de texte.




if __name__ == "__main__":

    game_info = init_pygame()
    timer = 1

    game_state = init_game()
    board, red_traps, blu_traps, shrimps, vil_red, vil_blu = game_state

    images = image_loader()

    if timer % 2 == 0:
        color = "red"
    else :
        color ="blue"
    
    if color == "red":
        vilain = vil_red[timer // 2]
    else:
        vilain = vil_blu[timer // 2]

    if color == "red":
        traps = red_traps
    else:
        traps = blu_traps

    draw_board_screen(board, game_info, images, shrimps)

    draw_board_traps(traps, game_info, images)

