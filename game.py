""""
GAME PYTHON MARCHE DU CRABE
"""

# Import des modules python
# import pygame

# Import des fonctions et variables du fichier lmdc
from lmdc import *
from ia_Axel import *

def main_forIA(): # Permet juste de lancer plus facilement
        
    game_state = init_game()

    return game_loop_std_ia(game_state)
    
def main():# Permet juste de lancer plus facilement
    game_state = init_game()

    game_loop_std(game_state)


def main_graphic():
  
    # Initialisation de Pygame, de la fenêtre de jeu et récupération des informations de diffusion
    game_info = Affichage_Pygame.init_pygame()

    timer = pygame.time.Clock() # pour actualiser le jeu

    
    
    # Chargement des images du jeu
    images = Affichage_Pygame.image_loader()

    # Lancement de la boucle du jeu

    # Initialisation du jeu
    game_state = init_game()
    game_loop_graphic(game_info, timer, images, game_state)


def game_loop_graphic(game_info, timer, images, game_state): 
    """
    Boucle principale du jeu pour avoir l'interface graphique
    
    Entrées :
        - informations sur fenêtre de jeu
        - L'horloge interne pygame pour le rafraîchissement du jeu
        - images du jeu
        - Etat du jeu 

        Sortie : Aucune 
    
    """

    round = 0 # Compteur nombre de round
    board, red_traps, blu_traps, shrimps, vil_red, vil_blu = game_state # Définitions des noms de variables à partir de game_state

    run = True
    while run:

        timer.tick(game_info["fps"]) # Contrôle la vitesse de rafraîchissement du jeu
        game_info["screen"].fill("white") # Fond d'écran de la fenêtre en blanc # Idée possible changer selon couleur du crabe

        
        # Gestion des évènements
        for event in pygame.event.get(): # Vérifie si l'utilisateur veut fermer la fenêtre ou pas
            if event.type == pygame.QUIT:
                run = False # Si c'est le cas, on arrête la boucle principale du jeu
        
        # Détermination de la couleur du joueur en fonction du tour
        if round % 2 == 0:
            color = "red"
        else :
            color ="blue"
        
        # Attribution du vilain suivant qui devra être ajouté sur le plateau
        if color == "red":
            vilain = vil_red[round // 2]
        else:
            vilain = vil_blu[round // 2]

        # Stockage des traps selon la couleur
        if color == "red":
            traps = red_traps
        else:
            traps = blu_traps

        col_vilain_position = Affichage_Pygame.ask_col_screen(board, color, traps, vilain, shrimps, game_info, images) # demande au joueur de placer le vilain
        
        pygame.display.flip() # Actualise l'affichage de la fenêtre de jeu
        game_info["screen"].fill("white") # Efface l'écran avec une couleur blanche

        put_vilain(board, vilain[1], col_vilain_position, vilain[0]) # Placement du vilain sur le plateau

        asked_position = Affichage_Pygame.ask_play_screen(board, color, traps, shrimps, images, game_info) # Demande au joueur de déplacer son pion

        pygame.display.flip() # Actualisation de l'affichage

        irow = asked_position[0] 
        icol = asked_position[1]
        
        life_lost = move(game_state, color, irow, icol) # Déplace le pion 

        shrimps -= life_lost # Actualise la perte de vies
        
        round += 1

        # Vérification si la partie est terminée et si le joueur a gagné
        if is_game_over(board, shrimps) == "Gagné":
            game_info["screen"].fill("white") # Ecran en blanc
            text_put_vilain = game_info["font"].render("Vous avez libéré vos camarades crabes verts, félicitation vous avez gagné la partie.", True, (0,0,0)) 
            game_info["screen"].blit(text_put_vilain, (game_info["middle_board"][0], game_info["middle_board"][1])) # Affiche le message de victoire
            pygame.time.delay(5000) # Délai avant de quittter la fenêtre
            run = False

        elif is_game_over(board,shrimps) == "Perdu":
            game_info["screen"].fill("white")
            text_put_vilain = game_info["font"].render("Vous avez libéré vos camarades crabes verts, félicitation vous avez gagné la partie.", True, (0,0,0)) 
            game_info["screen"].blit(text_put_vilain, (game_info["middle_board"][0], game_info["middle_board"][1])) # Affiche le message de défaite
            pygame.time.delay(5000) # Délai avant de quitter la fenêtre
            run = False
   
    pygame.quit() # Quitte la fenêtre


def game_loop_std(game_state): 
    """
    Lancer une partie complète jusqu'à ce qu'elle soit perdue ou gagnée
    """
         
    round = 0 # Compteur nombre de round
    board, red_traps, blu_traps, shrimps, vil_red, vil_blu = game_state
    
    
    run = True
    while run:
        # Mise en place de la coleur et avec le vilaine et les traps
        if round % 2 == 0:
            color = "red"
        else :
            color ="blue"
        
        if color == "red":
            vilain = vil_red[round// 2]
        else:
            vilain = vil_blu[round // 2]

        if color == "red":
            traps = red_traps
        else:
            traps = blu_traps
        

        col_vilain_position = ask_col(board, color, traps, shrimps, vilain) # Chiffre de la colonne du vilain
        # os.system('clear') # Permet de clear apres la première question 

        put_vilain(board, vilain[1], col_vilain_position, vilain[0]) # Placer le vilain

        asked_position = ask_play(board, color, traps, shrimps) # tuple ligne, colonne du mouvement du pion
        

        irow = int(asked_position[0])
        icol = int(asked_position[1])
        # os.system('clear')

        """deplace le pion """
        
        life_lost = move(game_state, color, irow, icol) # Bouger le pion et retourner le nombre vie perdue
        
        shrimps = shrimps - life_lost # Enlever des vies
    
        round += 1 
        
        # SI fin de partie 
        if is_game_over(board, shrimps) == "Gagné": 
            run = False
            return print("Les homards et tourteaux ont eu raison de vos crevettes, vous avez perdu la partie .")
            

        elif is_game_over(board, shrimps) == "Perdu":
            run = False
            return print("Les homards et tourteaux ont eu raison de vos crevettes, vous avez perdu la partie .")
            
        
def game_loop_std_ia(game_state): 
    """
    Lancer une partie complète jusqu'à ce qu'elle soit perdue ou gagnée
    """
         
    round = 0 # Compteur nombre de round
    board, red_traps, blu_traps, shrimps, vil_red, vil_blu = game_state
    
    execution_time = []
    run = True
    while run:
        # Mise en place de la coleur et avec le vilaine et les traps
        if round % 2 == 0:
            color = "red"
        else :
            color ="blue"
        
        if color == "red":
            vilain = vil_red[round// 2]
        else:
            vilain = vil_blu[round // 2]

        if color == "red":
            traps = red_traps
        else:
            traps = blu_traps
        

        col_vilain_position = ask_ia_col(board, color, traps, shrimps, vilain) # Chiffre de la colonne du vilain
        # os.system('clear') # Permet de clear apres la première question 

        put_vilain(board, vilain[1], col_vilain_position, vilain[0]) # Placer le vilain

        start_time = time.time()

        asked_position = ask_ia_play(board, color, traps, shrimps) # tuple ligne, colonne du mouvement du pion
        
        end_time = time.time()

        execution_time.append(end_time - start_time)


        irow = int(asked_position[0])
        icol = int(asked_position[1])
        # os.system('clear')

        """deplace le pion """
        
        life_lost = move(game_state, color, irow, icol) # Bouger le pion et retourner le nombre vie perdue
        
        shrimps = shrimps - life_lost # Enlever des vies
    
        round += 1 
        
        # SI fin de partie 
        if is_game_over(board, shrimps) == "Gagné": 
            run = False
            print("Les homards et tourteaux ont eu raison de vos crevettes, vous avez perdu la partie .")
            print("time_max:", max(execution_time), "tour", execution_time.index(max(execution_time)))
            return 1
            

        elif is_game_over(board, shrimps) == "Perdu":
            run = False
            print("Les homards et tourteaux ont eu raison de vos crevettes, vous avez perdu la partie .")
            print("time_max:", max(execution_time), "tour", execution_time.index(max(execution_time)))
            return 0

if __name__ == "__main__":
    victoire = 0
    for _ in range(30):
        etat = main_forIA()
        if etat == 1:
            victoire += 1
    print(victoire)

   