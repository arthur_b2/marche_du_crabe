import random
import lmdc
from io import StringIO
from pprint import pprint
import time
import copy

def name():
    """Say my name"""
    return "Krabby"

  
def ask_col_forIA(board, vilain, traps):
    """
    Retourne la possition calculée du vilain

    On place en priorité le vilain proche d'un traps
    """
    possible_positions = []
    offset = [(0, -1), (0, 1), (-1, 0), (1, 0)] # Gauche, droite, en haut, en bas
    
    # Trouver les traps et insérer les coordonnées autour dans une variable
    for irow in range(6):
        for icol in range(6):
            if board[irow][icol] == traps[0][0] or board[irow][icol] == traps[1][0]: # Lorsqu'il trouve un traps
                for x, y in offset:
                    possible_row = irow + x
                    possible_col = icol + y
                    if 0 <= possible_row <= 5 and 0 <= possible_col <= 5:
                        possible_positions.append((possible_row, possible_col)) # Ajouter les coordonnées dans un tuple pour chaque position
    
    # Si une des positions offset est possible ------------------------------------- 
    for coordonées_vil in possible_positions: # Pour chaque offset
        irow, icol = coordonées_vil
        if irow == vilain[1] and lmdc.is_free(board, irow, icol): # Si il est dans la même ligne que le vilain et que la place est libre
            return icol #retourner la colonne

    # Sinon -------------------------------------  

    _,position_col = lmdc.find_token(board) # Trouver la colonne de X et mettre le vilain comme premier choix a l'opposé
    if position_col > 2:
        for icol in [0,5,1,4,2,3]:
            if lmdc.is_free(board, vilain[1], icol) and board[vilain[1]][icol] != "X":
                return icol
    else :
        for icol in [5,0,4,1,3,2]:
            if lmdc.is_free(board, vilain[1], icol) and board[vilain[1]][icol] != "X":
                return icol                        

def ask_col_alea(board, vilain):
    """
    Tire aléatoirement une colonne possible 
    
    Entrées : Board et le vilain

    Sortie: la colonne 
    """

    pos_token_vilain = vilain[1]  # ligne vilain
    
    
    free_positions = [col for col in range(6) if lmdc.is_free(board, pos_token_vilain, col)] # Avoir toutes les possibilités 
    col_vilain_position = random.choice(free_positions) # Sélectionner aléatoirement parmi les possibilités.
        
    return col_vilain_position

def calculate_score(board, color, traps):
    """
    Creer un board_score puis choisi le meilleur coup

    A partir de la fonction calcul, calcule le premier coup puis le deuxieme par rapport au premier
    Ensuite on prend le max du second coup, cela nous donne une liste des premiers coups possibles et on prend le max dans cette liste pour choisir qu'elle sera la position a choisir
    """
    board_score = copy.deepcopy(board) # Copy le board
    position_row, position_col = lmdc.find_token(board) # Coord du pion
    
    def calcul(board, irow, icol, traps):
        """Donne un nombre a une case"""

        if board[irow][icol] == "   " or board[irow][icol] == " CV": # SI vide ou CV
            return 0
        if board[irow][icol] == "X  " or board[irow][icol] == "XCV" : # SI vide avec le pion 
            return -6
        elif board[irow][icol] in[" TB"," HB", " TR", " HR"]: # SI vilain 
            return -3
        elif board[irow][icol] in["XTB", "XHB", "XTR", "XHR"]: # SI X + vilain, -10 pour l'obliger a bouger 
            return -10 
        elif board[irow][icol] in (traps[0][0], traps[1][0], f"X{traps[0][0].replace(' ','')}", f"X{traps[1][0].replace(' ','')}"): # SI traps
            return -10
        else: # SInon reste que les obj
            return  8 
    
    ############## Creer un board_score ################
    # Calculer les cases possibles pour le premier coup, puis à partir d'elles, calculer tous les seconds coups possibles.
        
    if color == "red":
        # Calcule la colonne ou se trouve le pion --------------------------
        count = 0
        for irow in range(position_row, -1, -1): # Les lignes vers le haut -------------------------
            nb_case = calcul(board,irow,position_col, traps) # Avoir le nombre de la case
            if nb_case == -3: # SI la case est un vilain
                board_score[irow][position_col] = nb_case 
                count = -3 # Initialise count
            else : # SInon 
                board_score[irow][position_col] = nb_case + count # nb case + count, si pas de vilain avant alors count 0
            
        count = 0
        for irow in range(position_row, 6, 1): # Meme chose mais les lignes vers le bas --------------------------
            nb_case = calcul(board,irow,position_col, traps)
            if nb_case == -3:
                board_score[irow][position_col] = nb_case 
                count = -3
            else :
                board_score[irow][position_col] = nb_case + count
        
        # Calcule les lignes ----------------------------------
        for irow in range(6): # Calculer les lignes en partant de la colonne calculer juste au dessus 
            count = 0
            after_sum = board_score[irow][position_col] # C'est la case de depart du calcul. On se trouve sur une des lignes(irow) et dans la colonne calculée
            for icol in range(position_col-1, -1, -1): # Colonne vers la gauche  (position_col-1 pour pas changer l) 
                x = calcul(board,irow,icol, traps)
                if x == -3:
                    board_score[irow][icol] = x + after_sum 
                    count = -3
                else :
                    board_score[irow][icol] = x + count+ after_sum # Nombre de la case + count (si vilain sur le trajet) + la case de départ
                
            count = 0
            after_sum = board_score[irow][position_col]
            for icol in range(position_col + 1, 6, 1):  # Meme chose mais vers la droite
                x = calcul(board,irow,icol, traps)
                if x == -3:
                    board_score[irow][icol] = x + after_sum
                    count = -3
                else :
                    board_score[irow][icol] = x + count+ after_sum
    
    elif color == "blue": # Meme chose mais en commencant par calculer la ligne puis les colonnes car c'est bleu qui joue -------------------------------
        count = 0
        for icol in range(position_col, -1, -1):
            x = calcul(board, position_row, icol, traps)
            if x == -3:
                board_score[position_row][icol] = x
                count = -3
            else:
                board_score[position_row][icol] = x + count

        count = 0
        for icol in range(position_col, 6, 1):
            x = calcul(board, position_row, icol, traps)
            if x == -3:
                board_score[position_row][icol] = x
                count = -3
            else:
                board_score[position_row][icol] = x + count

        for icol in range(6):
            count = 0
            after_sum = board_score[position_row][icol]
            for irow in range(position_row - 1, -1, -1):

                x = calcul(board, irow, icol, traps)
                if x == -3:
                    board_score[irow][icol] = x + after_sum
                    count = -3
                else:
                    board_score[irow][icol] = x + count+ after_sum


            count = 0
            after_sum = board_score[position_row][icol]
            for irow in range(position_row + 1, 6, 1):

                x = calcul(board, irow, icol, traps)
                if x == -3:
                    board_score[irow][icol] = x + after_sum
                    count = -3
                else:
                    board_score[irow][icol] = x + count + after_sum 

    ################ Choisir ou aller d'apres le board_score ###########
                    
    if color == "red": # Obtient l'indice des lignes les plus intéressantes
        max_row = [max(row) for row in board_score]  # Prendre la valeur maximale de chaque ligne
        max_value = max(max_row) # Prend la valeur max de max_row
        indices = [irow for irow, value in enumerate(max_row) if value == max_value] # donne l'indice ou se trouve les maximum de max_row
        
    else: # Obtient l'indice des colonnes les plus intéressantes
        max_col = [max(col) for col in zip(*board_score)] # zip(*tableau) permet de transformer le tableau en ensemble de liste de ligne --> en ensemble de liste de colonne. 
        # * --> décompactage des arguments d'une liste --> permet d'avoir chaque élément comme argument séparé à la fonction 
        max_value = max(max_col)
        indices = [icol for icol, value in enumerate(max_col) if value == max_value]
    return random.choice(indices) # Random car il peut avoir 2 lignes ou colonnes avec le meme max 

def ask_play_forIA(board, color, traps): 
    """
    Fonction pour demander à l'IA de se déplacer 

    Entrées : 
        - plateau de jeu
        - couleur du crabe
        - les pièges
    
    Sortie :
        - tuple contenant la case où l'IA veut se déplacer
    """

    move_valid = False # Tant que le déplacement voulu n'est pas correct
    while move_valid == False:

        start = lmdc.find_token(board) # coordonnées du pion
        

        if color == "red":
            irow = calculate_score(board, color, traps) # Calcul le score et renvoie la ligne de la meilleure possibilité selon calculate_score
            end = (irow, start[1]) # Tuple position finale
            if lmdc.is_valid_move(board, color, start, end) == True: # Vérifie que le déplacement est valide
                move_valid = True
                return end
        
        if color == "blue":
            icol = calculate_score(board, color, traps) # Calcul le score et renvoie la ligne de la meilleure possibilité selon calculate_score
            # if icol != type(int): # A REVOIR 
            #     pass
            end = (start[0], icol) # Tuple position finale
            if lmdc.is_valid_move(board, color, start, end) == True: # Vérifie que le déplacement est valide
                move_valid = True
                return end

def ask_play_alea(board, color):  
    """
    Fonction qui génère un déplacement aléatoire pour les simulations de party_generation

    Entrées : 
        - plateau de jeu
        - couleur du crabe
    
    Sortie :
        - tuple des coordonnées de la case finale
    """  
    irow,icol = lmdc.find_token(board) 
    return (random.randint(0, 5) , icol) if color == "red" else (irow ,random.randint(0, 5)) #  si rouge on conserve icol et tirage aléatoire ligne, bleu c'est l'inverse


def party_generation(board, color, traps, shrimps):
    """
    Génère une partie complète à partir d'un état donné

    Entrées : 
        - état du jeu
        - nombre de tours
    
    Sortie :
        - first_row : ligne du déplacement du premier tour
        - first_col : colonne du déplacement du premier tour
        - vilain_placed_IA[0] : Colonne du vilain posé au premieer tour
        - step_board : plateau à la fin de la partie
        - step_shrimps : nombre de vies à la fin de la partie
        - round : nombre de tours joués sur la partie entière

    """
    
    # Définitions de variables
    vilain_placed_IA = [] # contiendra les colonnes des vilains placés
    
    step_vil_red = [( " TR" ,0) ,( " TR" ,0) ,( " TR" ,1) ,( " HR" ,1) ,( " TR" ,2) ,( " TR" ,2) ,
    ( " TR" ,3) ,( " HR" ,3) ,( " TR" ,4) ,( " TR" ,4) ,( " TR" ,5) ,( " HR" ,5)]
    random.shuffle(step_vil_red)
    
    step_vil_red_X = [("XTR",0),("XTR",0),("XTR",1),("XHR",1),("XTR",2),("XTR",2),
("XTR",3),("XHR",3),("XTR",4),("XTR",4),("XTR",5),("XHR",5)
]
    step_vil_red_copy = copy.deepcopy(step_vil_red)

    step_vil_blue = [( " TB" ,0) ,( " HB" ,0) ,( " TB" ,1) ,( " TB" ,1) ,( " TB" ,2) ,( " HB" ,2) ,
    ( " TB" ,3) ,( " TB" ,3) ,( " TB" ,4) ,( " HB" ,4) ,( " TB" ,5) ,( " TB" ,5) ]
    step_vil_blue_X = [( "XTB" ,0) ,( "XHB" ,0) ,( "XTB" ,1) ,( "XTB" ,1) ,( "XTB" ,2) ,( "XHB" ,2) ,
    ( "XTB" ,3) ,( "XTB" ,3) ,( "XTB" ,4) ,( "XHB" ,4) ,( "XTB" ,5) ,( "XTB" ,5) ]

    random.shuffle(step_vil_blue)

    all_vil_blue = step_vil_blue + step_vil_blue_X
    all_vil_red = step_vil_red + step_vil_red_X

    for irow in range(len(board)): # Regarde les vilains restants à l'état 0 
        for icol in range(len(board[0])):
            if ((board[irow][icol], irow) in step_vil_red or (board[irow][icol], irow) in step_vil_red_X) and (irow,icol) not in global_obj_pos:
                step_vil_red_copy.remove((board[irow][icol].replace("X"," "), irow))
            if ((board[irow][icol], irow) in step_vil_blue or (board[irow][icol], irow) in step_vil_blue_X) and (irow,icol) not in global_obj_pos:
                step_vil_blue.remove((board[irow][icol].replace("X"," "), irow))


     # Predire aléatorirement les traps de notre teammeate (m8) -------------------------------------------------
    
    OBJ_LIST = [
    ( " PS" ,0) ,( " ME" ,0) ,( " GC" ,1) ,( " BE" ,1) ,( " PN" ,2) ,( " BM" ,2) ,
    ( " BC" ,3) ,( " LG" ,3) ,( " BV" ,4) ,( " CT" ,4) ,( " BL" ,5) ,( " SP" ,5)
    ]

    OBJ_LIST.remove(traps[0]) # Enlever nos traps
    OBJ_LIST.remove(traps[1]) 

    count_CV = 0
    count_obj = 0
    obj_restant = []
    for irow in range(len(board)):
        for icol in range(len(board[0])):
            if board[irow][icol] == " CV" or board[irow][icol] == "XCV":
                count_CV += 1
            if (board[irow][icol], irow) in OBJ_LIST:
                count_obj +=1
                obj_restant.append(board[irow][icol]) # Tous les obj encore present sur le board


    if (count_CV + count_obj) == 10: #Nombre de trap_m8 pas retourner 
        count_traps = 2
    if (count_CV + count_obj) == 9:
        count_traps = 1
    if (count_CV + count_obj) == 8:
        count_traps = 0
        
    #(count_CV + count_obj) = 10 -> 2 traps
    # si = 9 -> 1 traps
    # si = 8 -> 0 traps
    traps_m8 = []
    
    if count_traps > 0: # Si au moins un trap pas retourner 
        traps1 = random.choice(obj_restant) # tire aléatoirment 
        obj_restant.remove(traps1) # enleve de la liste
        for item in OBJ_LIST:
            if item[0] == traps1:
                traps_m8.append(item)
                 
    if count_traps > 1: # Meme si chose si aucun traps n'a etait retoruner 
        traps2 = random.choice(obj_restant)
        obj_restant.remove(traps2)
        for item in OBJ_LIST:
            if item[0] == traps2:
                traps_m8.append(item)


    nb_tour = 24 - (len(step_vil_blue) + len(step_vil_red_copy)) 
    
    run = True
    round = 0
    count = 0 # Pour ne prendre que les premières coordonnées du déplacement du joueur au premier tour
    ######################### Commencer la boucle jusqu'à la fin du jeu------------------------------------------------     

    while run: 
    
        if nb_tour % 2 == 0:
            color_now = "red"
            vilain = step_vil_red_copy[round // 2]
            traps_red = traps
            traps_blue = traps_m8

        else:
            color_now = "blue"
            vilain = step_vil_blue[round // 2]
            traps_blue = traps
            traps_red = traps_m8
    
        
        step_board = board
        step_shrimps = shrimps 
        game_state = (step_board, traps_red, traps_blue, step_shrimps, step_vil_red, step_vil_blue)

        
        # ;AU bout du 10eme tour, je redéfinis les traps ---------------------------------------------
        if nb_tour == 10:
            
            offset = [(0, -1), (0, 1), (-1, 0), (1, 0)] # Gauche, droite, en haut, en bas
            
            # Trouver les traps et insérer les coordonnées autour dans une variable
            obj_score = []
            for irow in range(6):
                for icol in range(6):
                    if board[irow][icol] in obj_restant: # Obj restant pas nos traps normalement
                        # Compter le nombre de vilains autour de chaque objet restant
                        count_vilain = 0
                        for x,y in offset:
                            possible_row = irow + x
                            possible_col = icol + y
                            if 0 <= possible_row <= 5 and 0 <= possible_col <= 5:
                                if (board[irow+x][icol+y], irow) in all_vil_blue:
                                        count_vilain += 1
                        obj_score.append((board[irow][icol], count_vilain))
            
            count_traps = 10 - (count_CV + count_obj) # Nombre de trap_m8 pas retourner
            traps_m8 = []
            if count_traps > 0: # Si au moins un trap pas retourner 
                
                list_decroissante_nb_vilain = sorted(obj_score, key=lambda x: x[1], reverse=True) # la liste de facon decroissante
                traps_m8.append(list_decroissante_nb_vilain[0]) # tire le premier 
                
            if count_traps > 1: # Meme si chose si aucun traps n'a etait retoruner 
                traps_m8.append(list_decroissante_nb_vilain[1])# tire le deuxieme 

        elif nb_tour == 11:
            offset = [(0, -1), (0, 1), (-1, 0), (1, 0)] # Gauche, droite, en haut, en bas
            
            # Trouver les traps et insérer les coordonnées autour dans une variable
            obj_score = []
            for irow in range(6):
                for icol in range(6):
                    
                    if board[irow][icol] in obj_restant: # Obj restant pas nos traps normalement
                        # Compter le nombre de vilains autour de chaque objet restant
                        count_vilain = 0
                        for x, y in offset:# Critère Pour éviter dépassement plateau
                            possible_row = irow + x
                            possible_col = icol + y
                            if 0 <= possible_row <= 5 and 0 <= possible_col <= 5:
                                if (board[irow+x][icol+y], irow) in all_vil_red:
                                    count_vilain += 1
                        obj_score.append((board[irow][icol], count_vilain))
            count_traps = 10 - (count_CV + count_obj) # Nombre de trap_m8 pas retourner
            traps_m8 = []
            if count_traps > 0: # Si au moins un trap pas retourner 
                
                list_decroissante_nb_vilain = sorted(obj_score, key=lambda x: x[1], reverse=True) # la liste de facon decroissante
                traps_m8.append(list_decroissante_nb_vilain[0]) # tire le premier 
                
            if count_traps > 1: # Meme si chose si aucun traps n'a etait retoruner 
                traps_m8.append(list_decroissante_nb_vilain[1])# tire le deuxieme 
                
        
        
        ############### Attribuer des nouveau traps au M8(mate) avec offset (plusieur biais : si sur extrémité et lorsqu'un traps se transforme en TR)
        
        # Si couleur du joueur alors jeu précis mais partenaire aléatoire
        
        if color == color_now : # Si couleur du bot alors il joue selon nos conditions sinon aléatoire
            
            col_vilain_position = ask_col_forIA(step_board, vilain,traps) # Commande de l'IA Avec consignes
            vilain_placed_IA.append(col_vilain_position) # Ajoute dans une liste pour récupérer le premier plus tard
            
            lmdc.put_vilain(step_board, vilain[1], col_vilain_position, vilain[0]) # Place le vilain

            asked_position = ask_play_forIA(step_board, color_now, traps) # Déplacement de l'IA selon les consignes 

        else: # au tour de l'adversaire qui joue aléatoirement ses coups
            
            col_vilain_position = ask_col_alea(step_board, vilain) # Tirage aléatoire colonne pour placer le vilain
            vilain_placed_IA.append(col_vilain_position) # Mémorise la colonne utilisée

            lmdc.put_vilain(step_board, vilain[1], col_vilain_position, vilain[0]) # Place le vilain

            asked_position = ask_play_alea(step_board, color_now) # Tirage d'une case aléatoire en respectant les consignes du jeu

        irow, icol = asked_position 
        while count == 0: # Conserve seulement les coordonnées du premier déplacement
            first_row = int(irow)
            first_col = int(icol)
            count +=1
            
        life_lost = lmdc.move(game_state, color_now, irow, icol) # Mise à jour des déplacements


        step_shrimps = step_shrimps - life_lost # Mise à jour des vies
            
        nb_tour +=1
        round += 1
        
        if lmdc.is_game_over(step_board, step_shrimps) == "Gagné":
            run = False
            return first_row,first_col, vilain_placed_IA[0], step_board, step_shrimps, round

        elif lmdc.is_game_over(step_board, step_shrimps) == "Perdu":
            run = False
            return first_row,first_col, vilain_placed_IA[0], step_board, step_shrimps, round
        
def calcul_score(board, shrimps, round):
    """
    Calcul du score final d'une partie
    
    Une partie gagnée : + 10
    Une partie perdue : -5
    Crabe découvert : +3 / CV
    Nb de vie : -3 par vie perdue
    Nb de round : s'ajoutera après surement.

    
    Entrées :
        - plateau de jeu
        - vies
        - nombre de tours

    Sortie : 
        - score de la partie
    """

    score = 0

    # Etat de la partie + round

    if lmdc.is_game_over(board, shrimps) == "Gagné":
        score += 100
        score += 100 / round
        
    else:
        score -= 100

    # Vie perdue

    score += (5 - shrimps) * -3

    # CV découvert
    crabe_vert = 0

    for irow in range(len(board)):
        crabe_vert += sum(board[irow][icol] == " CV" or board[irow][icol] == "XCV" for icol in range(len(board)))

    score += crabe_vert * 3
    
    return score

global_obj_pos = set()

def pos_OBJECT(board):
            OBJ_LIST = [
            ( " PS" ,0) ,( " ME" ,0) ,( " GC" ,1) ,( " BE" ,1) ,( " PN" ,2) ,( " BM" ,2) ,
            ( " BC" ,3) ,( " LG" ,3) ,( " BV" ,4) ,( " CT" ,4) ,( " BL" ,5) ,( " SP" ,5)
            ]
            global global_obj_pos
            for irow in range(len(board)):
                for icol in range(len(board[0])):
                    if (board[irow][icol], irow) in OBJ_LIST:
                        global_obj_pos.add((irow,icol))            
            return global_obj_pos



def ia_playing(board, color, traps, shrimps): #game sate round
    """
    Tour de jeu de l'IA
    
    Entrées :
        - état du jeu
        - nombre de tours

    Sortie :
        - all_asked_position[idx_best_party] : coordonnées [row,col] de la meilleure partie simulée
        - all_vilain_placed_IA[idx_best_party] : colonne du meilleur placement de vilain sur les parties simulées
    """

    # Définitions des variables
    
    all_asked_position = [] # Contient toutes les premières positions simulées pour la fonction déplacement pour chaque partie simulée
    all_vilain_placed_IA = [] # Contient toutes les premières colonnes simulées pour le premier vilain placé à chaque partie simulée.
    all_score = [] # Contient l'ensemble des scores pour chaque partie jouée
  
    # all_object_position = pos_OBJECT(board)
    count = 0
    for irow in range(6):
        for icol in range(6):
            if lmdc.is_free(board, irow, icol) == True:
                count +=1

    nb_de_tours = 24 - count

    if nb_de_tours < 8:
        nb_simulation = 100 + nb_de_tours * 50
    else : 
        nb_simulation = 100 + nb_de_tours * 50

     # Le nombre de simulation augmentera avec le temps (car plus on avance plsu le temps de calcul est faible donc on peit rajouter des simu)
    print("nb simulation :", nb_simulation)

    if nb_de_tours == 1:
        global global_obj_pos
        global_obj_pos = set()
        pos_OBJECT(board)

    OBJ_LIST = [
            ( " PS" ,0) ,( " ME" ,0) ,( " GC" ,1) ,( " BE" ,1) ,( " PN" ,2) ,( " BM" ,2) ,
            ( " BC" ,3) ,( " LG" ,3) ,( " BV" ,4) ,( " CT" ,4) ,( " BL" ,5) ,( " SP" ,5)
            ]
    
    if nb_de_tours == 24:
        irow,col = lmdc.find_token(board)
        if icol in range(6):
            if (board[irow][icol], irow) in OBJ_LIST:
                return (irow,icol)
        return (irow,col)

    
    for _ in range(nb_simulation):

        copy_board, copy_color, copy_traps, copy_shrimps = copy.deepcopy(board), copy.deepcopy(color), copy.deepcopy(traps), copy.deepcopy(shrimps) # copie de l'état du jeu pour recommencer la génération de parties
        
        row, col, vilain_placed_IA, step_board, step_shrimps, step_round = party_generation(copy_board, copy_color, copy_traps, copy_shrimps) # Génère les parties à partir de l'état du plateau
        
        # all_asked_position.append(asked_position)
        all_asked_position.append([row,col]) # stock les indices du premier déplacement de chaque partie simulée
        all_vilain_placed_IA.append(vilain_placed_IA) # Stock les indices du premier vilain placé pour chaque partie simulée

        score = calcul_score(step_board, step_shrimps, step_round) # Calcul d'un score sur chaque plateau de fin de partie pour chaque partie simulée
        all_score.append(score)
        
    
    # Avec tous les scores, chercher le plus grand, on prend le premier
    
    idx_best_party = all_score.index(max(all_score)) # Prend l'indice du premier meilleur score
    
    return all_asked_position[idx_best_party]


def ask_ia_col (board, color, traps, shrimps, vilain):
    return ask_col_forIA(board, vilain, traps)

def ask_ia_play(board, color, traps, shrimps):
    move_ia= ia_playing(board, color, traps, shrimps)
    return move_ia


if __name__ == "__main__":
    
    game_state = lmdc.init_game()
    board, red_traps, blu_traps, shrimps, vil_red, vil_blu = game_state

    board = [
        ["   ", " PS", "   ", "   ", "   ", " ME"],
        ["   ", " BE", " GC", "   ", "   ", "   "],
        ["   ", "   ", " BM", " PN", "   ", "   "],
        [" LG", "   ", "X  ", "   ", " BC", "   "],
        ["   ", "   ", "   ", " CT", " BV", "   "],
        ["   ", "   ", "   ", " SP", "   ", " BL"]
    ]

    color = "blue"
    traps = [(' CT', 4), (' GC', 1)]
    vilain = (' TR', 3)
    round = 0

    

    if round % 2 == 0:
        color = "red"
    else :
        color ="blue"
    
    if color == "red":
        vilain = vil_red[round // 2]
    else:
        vilain = vil_blu[round // 2]

    if color == "red":
        traps = red_traps
    else:
        traps = blu_traps

    #Tmps : 
    start_time = time.time()

    print(ask_ia_play(board, color, traps, shrimps))

    end_time = time.time()

    execution_time = end_time - start_time

    print("Tps :", execution_time, "secondes")
