"""
    LMDC TESTS
"""

import pytest
import os
import sys
import mock
import builtins
import pprint
from unittest.mock import patch
from unittest import TestCase
import time
from io import StringIO
from copy import deepcopy
# import capsys

#import monkeypatch

lmdc = pytest.importorskip("lmdc")

game_state = lmdc.init_game()
board, traps , _, shrimps, vilain,_ = game_state

board_test = [
    ["   ", " PS", "   ", "   ", "   ", " ME"],
    ["   ", " BE", " GC", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " CV"],
    [" LG", "   ", "   ", "   ", " BC", "   "],
    ["   ", "   ", "   ", " CT", " BV", "   "],
    ["X  ", "   ", "   ", " SP", "   ", " BL"]
]


# [0][0]: is free: T
# [0][1]: is free: F
# [2][0]: Vilain
# [2][5]: Crabe vert
#raps2test = [(  apres ask col " ME" ,0),( " GC" ,1)]


#class Test_init_game:

#class Test_str_game: 
#regarder si print bien le plateau
result = lmdc.find_token(board)
class Test_find_token:
    def test_sortie(self): 
        """Return un tuple de longeur 2"""
        assert isinstance(result, tuple) and len(result) == 2
    def test_coordonnees_possible(self):
        """Le irow et le icol sont bien entre 0 et 5"""
        assert result[0] in range(6) and result[1] in range(6) 
    def test_pion_present(self):
        """Dans cette postition il y a bien un X """
        assert "X" in board[result[0]][result[1]] 


class Test_is_free:
    def test_sortie(self):
        """Test plusieurs endroits sur le plaeau pour savoir s'ils sont libres ou non"""
        board_test = [
            ["   ", " PS", "   ", "   ", "   ", " ME"],
            ["   ", " BE", " GC", "   ", "   ", "   "],
            [" TR", "   ", " BM", " PN", "   ", " CV"],
            [" LG", "   ", "   ", "   ", " BC", "   "],
            ["   ", "   ", "   ", " CT", " BV", "   "],
            ["X  ", "   ", "   ", " SP", "   ", " BL"]
        ]
        assert lmdc.is_free(board_test,0,0) == True
        assert lmdc.is_free(board_test,0,1) == False
        
    # On peut rien tester de plus ici

# FOR CAPTURE SYS : https://pavolkutaj.medium.com/how-to-test-printed-output-in-python-with-pytest-and-its-capsys-fixture-161010cfc5ad
    
class Test_ask_col:
    def test_sortie(self,monkeypatch):
        """"sortie est int et la case est bien vide"""
        board_test = [
    ["   ", " PS", "   ", "   ", "   ", " ME"],
    ["   ", " BE", " GC", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " CV"],
    [" LG", "   ", "   ", "   ", " BC", "   "],
    ["   ", "   ", "   ", " CT", " BV", "   "],
    ["X  ", "   ", "   ", " SP", "   ", " BL"]]
        

        color = "red"
        vilain_now = (" TR" , 3)
        monkeypatch.setattr("sys.stdin", StringIO("2\na2\n2B\n1040\nOK\nGZ\nG10\nA2\nC\n"))
        resultat = lmdc.ask_col( board_test , color , traps , shrimps , vilain_now) 
        print(resultat)
        assert isinstance(resultat, int) and 0 <= resultat <= 5 # resultat = a un entier entre 0 et 5
        assert board_test[vilain_now[1]][resultat] == "   " # est ce que la case est bien vide

    # def test_correct_output(self):

    #     board_test = [
    # ["   ", " PS", "   ", "   ", "   ", " ME"],
    # ["   ", " BE", " GC", "   ", "   ", "   "],
    # [" TR", "   ", " BM", " PN", "   ", " CV"],
    # [" LG", "   ", "   ", "   ", " BC", "   "],
    # ["   ", "   ", "   ", " CT", " BV", "   "],
    # ["X  ", "   ", "   ", " SP", "   ", " BL"]]
        
    #     color = "red"
        

    #     expected_output = 


# def put_vilain(board, irow, icol, vilain):
class Test_put_vilain:
    def test_sortie(self):
        """Test placement d'un vilain dans le plateau"""
        board_test = [
    ["   ", " PS", "   ", "   ", "   ", " ME"],
    ["   ", " BE", " GC", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " CV"],
    [" LG", "   ", "   ", "   ", " BC", "   "],
    ["   ", "   ", "   ", " CT", " BV", "   "],
    ["X  ", "   ", "   ", " SP", "   ", " BL"]]
        
        lmdc.put_vilain(board_test,0,4,"TR")
        assert board_test[0][4] == "TR"
        
    def test_vilain_sur_pion(self):
        """Le pion ne disparait pas """
        board_test = [
    ["   ", " PS", "   ", "   ", "   ", " ME"],
    ["   ", " BE", " GC", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " CV"],
    [" LG", "   ", "   ", "   ", " BC", "   "],
    ["   ", "   ", "   ", " CT", " BV", "   "],
    ["X  ", "   ", "   ", " SP", "   ", " BL"]]
        lmdc.put_vilain(board_test,5,0,"TR")
        assert board_test[5][0] == "XTR"



class Test_is_valid_move:
    
    def test_wrong_move_red(self):
        """Mauvais déplacement rouge"""
        board_test = [
    ["   ", " PS", "   ", "   ", "   ", " ME"],
    ["   ", " BE", " GC", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " CV"],
    [" LG", "   ", "   ", "   ", " BC", "   "],
    ["   ", "   ", "   ", " CT", " BV", "   "],
    ["X  ", "   ", "   ", " SP", "   ", " BL"]]
        
        assert lmdc.is_valid_move(board_test, "red", (5,0), (5,4)) == False
    def test_wrong_move_blue(self):
        """Mauvais déplacement bleu"""
        board_test = [
    ["   ", " PS", "   ", "   ", "   ", " ME"],
    ["   ", " BE", " GC", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " CV"],
    [" LG", "   ", "   ", "   ", " BC", "   "],
    ["   ", "   ", "   ", " CT", " BV", "   "],
    ["X  ", "   ", "   ", " SP", "   ", " BL"]]
        
        assert lmdc.is_valid_move(board_test, "blue", (5,0), (3,0)) == False
    def test_dont_move(self):
        board_test = [
    ["   ", " PS", "   ", "   ", "   ", " ME"],
    ["   ", " BE", " GC", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " CV"],
    [" LG", "   ", "   ", "   ", " BC", "   "],
    ["   ", "   ", "   ", " CT", " BV", "   "],
    ["X  ", "   ", "   ", " SP", "   ", " BL"]]
        
        assert lmdc.is_valid_move(board_test, "red", (5,0), (5,0)) == True
    
    def test_out_map(self):
        board_test = [
    ["   ", " PS", "   ", "   ", "   ", " ME"],
    ["   ", " BE", " GC", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " CV"],
    [" LG", "   ", "   ", "   ", " BC", "   "],
    ["   ", "   ", "   ", " CT", " BV", "   "],
    ["X  ", "   ", "   ", " SP", "   ", " BL"]]
        
        assert lmdc.is_valid_move(board_test, "red", (5,0), (18,18)) == False
        assert lmdc.is_valid_move(board_test, "red", (5,0), (-10,-6)) == False
    
    def test_good_move_red(self):
        """Bon déplacement rouge"""
        board_test = [
    ["   ", " PS", "   ", "   ", "   ", " ME"],
    ["   ", " BE", " GC", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " CV"],
    [" LG", "   ", "   ", "   ", " BC", "   "],
    ["   ", "   ", "   ", " CT", " BV", "   "],
    ["X  ", "   ", "   ", " SP", "   ", " BL"]]
        
        assert lmdc.is_valid_move(board_test, "red", (5,0), (3,0)) == True

    def test_good_move_blue(self):
        """Bon déplacement bleu"""
        board_test = [
    ["   ", " PS", "   ", "   ", "   ", " ME"],
    ["   ", " BE", " GC", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " CV"],
    [" LG", "   ", "   ", "   ", " BC", "   "],
    ["   ", "   ", "   ", " CT", " BV", "   "],
    ["X  ", "   ", "   ", " SP", "   ", " BL"]]
        
        assert lmdc.is_valid_move(board_test, "blue", (5,0), (5,2)) == True

class Test_ask_play:
    def test_good_move(self, monkeypatch):
        """Bonne réponse et ajout dans board correct"""
        board_test = [
    ["   ", " PS", "   ", "   ", "   ", " ME"],
    ["   ", " BE", " GC", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " CV"],
    [" LG", "   ", "   ", "   ", " BC", "   "],
    ["   ", "   ", "   ", " CT", " BV", "   "],
    ["X  ", "   ", "   ", " SP", "   ", " BL"]]
        
        # Bon plateau et bon déplacement => test du tuple de fin
        simulated_input = StringIO("A2\n")

        monkeypatch.setattr('sys.stdin', simulated_input)
        res = lmdc.ask_play(board_test, "red", traps, shrimps)

        assert res == (2,0)


    def test_wrong_rep_enter(self, monkeypatch):
        """Test de plusieurs fausses entrées"""
        board_test = [
    ["   ", " PS", "   ", "   ", "   ", " ME"],
    ["   ", " BE", " GC", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " CV"],
    [" LG", "   ", "   ", "   ", " BC", "   "],
    ["   ", "   ", "   ", " CT", " BV", "   "],
    ["X  ", "   ", "   ", " SP", "   ", " BL"]]
        
        simulated_input2 = "A\na2\n2B\n1040\nOK\nGZ\nG10\nA2\n"
        

        monkeypatch.setattr('sys.stdin', StringIO(simulated_input2))
        res = lmdc.ask_play(board_test, "red", traps, shrimps)
        
        assert res == (2,0)
        # si il renvoie le truc alors pas bon

    def test_wrong_movement_red(self, monkeypatch):
        """Bon déplacement en rouge avec un test négatif avant"""
        board_test = [
    ["   ", " PS", "   ", "   ", "   ", " ME"],
    ["   ", " BE", " GC", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " CV"],
    [" LG", "   ", "   ", "   ", " BC", "   "],
    ["   ", "   ", "   ", " CT", " BV", "   "],
    ["X  ", "   ", "   ", " SP", "   ", " BL"]]
        
        
        # Tester si les couleurs sont prises en compte

        simulated_input = "E5\nA3\n"

        monkeypatch.setattr('sys.stdin', StringIO(simulated_input))

        res = lmdc.ask_play(board_test, "red", traps, shrimps)

        assert res == (3, 0)

    def test_wrong_movement_blue(self, monkeypatch):
        """Bon déplacement en bleu avec un test négatif avant"""
        board_test = [
    ["   ", " PS", "   ", "   ", "   ", " ME"],
    ["   ", " BE", " GC", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " CV"],
    [" LG", "   ", "   ", "   ", " BC", "   "],
    ["   ", "   ", "   ", " CT", " BV", "   "],
    ["X  ", "   ", "   ", " SP", "   ", " BL"]]
        
            
        # Tester si les couleurs sont prises en compte

        simulated_input = "A3\nE5\n"

        monkeypatch.setattr('sys.stdin', StringIO(simulated_input))

        res = lmdc.ask_play(board_test, "blue", traps, shrimps)

        assert res == (5, 4)


class Test_move:

         
        # Passage  sur vilain (-1)
        # Passage sur traps (-2)
        # Passage sur 3 vilains (-1)
        # Tester si le problème existe pour chaque couleur

        # Placer le X sur vilain
        # Regarder le changement de al case -> "CV" "Vilain"



    def test_move_dont_move(self):
        """Ne pas bouger"""
        board_test_move = [
    ["   ", "   ", "   ", "   ", "   ", "   "],
    [" CV", "   ", "   ", "   ", "   ", "   "],
    [" TR", "   ", "   ", "   ", "   ", "   "],
    [" LG", "   ", "   ", "   ", "   ", "   "],
    ["   ", "   ", "   ", "   ", "   ", "   "],
    ["X  ", "   ", " BL", " ME", " HB", "   "]]

        traps_test = [( " ME" ,0),( " GC" ,1)]
        game_state_test = (board_test_move,traps_test, game_state[2],game_state[3], game_state[4], game_state[5]  )

        assert lmdc.move(game_state_test, "red", 5,0) == 0


    def test_move_empty(self):
        """Bouger le pion sur une case vide, regarder que le nb de point de vie perdu = 0 et regarder si le board à la fin correspond"""
        board_test_move = [
    ["   ", "   ", "   ", "   ", "   ", "   "],
    [" CV", "   ", "   ", "   ", "   ", "   "],
    [" TR", "   ", "   ", "   ", "   ", "   "],
    [" LG", "   ", "   ", "   ", "   ", "   "],
    ["   ", "   ", "   ", "   ", "   ", "   "],
    ["X  ", "   ", " BL", " ME", " HB", "   "]]

        traps_test = [( " ME" ,0),( " GC" ,1)]
        game_state_test = (board_test_move,traps_test, game_state[2],game_state[3], game_state[4], game_state[5]  )

   
        assert lmdc.move(game_state_test, "red", 5,1) == 0

        board_end = [
            ["   ", "   ", "   ", "   ", "   ", "   "],
            [" CV", "   ", "   ", "   ", "   ", "   "],
            [" TR", "   ", "   ", "   ", "   ", "   "],
            [" LG", "   ", "   ", "   ", "   ", "   "],
            ["   ", "   ", "   ", "   ", "   ", "   "],
            ["   ", "X  ", " BL", " ME", " HB", "   "]]
        
        assert board_test_move == board_end 

    
        
        
    def test_move_one_vilain(self):
        """Bouger sur un vilain HB, vie perdu == 1"""
        board_test_move = [
    ["   ", "   ", "   ", "   ", "   ", "   "],
    [" CV", "   ", "   ", "   ", "   ", "   "],
    [" TR", "   ", "   ", "   ", "   ", "   "],
    [" LG", "   ", "   ", "   ", "   ", "   "],
    ["   ", "   ", "   ", "   ", "   ", "   "],
    ["X  ", "   ", " BL", " ME", " HB", "   "]]

        traps_test = [( " ME" ,0),( " GC" ,1)]
        game_state_test = (board_test_move,traps_test, game_state[2],game_state[3], game_state[4], game_state[5]  )

   
        assert lmdc.move(game_state_test, "red", 5,4) == 1

        board_end = [
            ["   ", "   ", "   ", "   ", "   ", "   "],
            [" CV", "   ", "   ", "   ", "   ", "   "],
            [" TR", "   ", "   ", "   ", "   ", "   "],
            [" LG", "   ", "   ", "   ", "   ", "   "],
            ["   ", "   ", "   ", "   ", "   ", "   "],
            ["   ", "   ", " BL", " ME", "XHB", "   "]]
        
        assert board_test_move == board_end 
        
        
        
    def test_move_one_traps(self):
        """Bouger sur traps, vie perdu = 1 """
        board_test_move = [
    ["   ", "   ", "   ", "   ", "   ", "   "],
    [" CV", "   ", "   ", "   ", "   ", "   "],
    [" TR", "   ", "   ", "   ", "   ", "   "],
    [" LG", "   ", "   ", "   ", "   ", "   "],
    ["   ", "   ", "   ", "   ", "   ", "   "],
    ["X  ", "   ", " BL", " ME", " HB", "   "]]

        traps_test = [( " ME" ,0),( " GC" ,1)]
        game_state_test = (board_test_move,traps_test, game_state[2],game_state[3], game_state[4], game_state[5]  )

        assert lmdc.move(game_state_test, "red", 5,3) == 1

        board_end = [
            ["   ", "   ", "   ", "   ", "   ", "   "],
            [" CV", "   ", "   ", "   ", "   ", "   "],
            [" TR", "   ", "   ", "   ", "   ", "   "],
            [" LG", "   ", "   ", "   ", "   ", "   "],
            ["   ", "   ", "   ", "   ", "   ", "   "],
            ["   ", "   ", " BL", "XTB", " HB", "   "]]
        board_end2 = [
            ["   ", "   ", "   ", "   ", "   ", "   "],
            [" CV", "   ", "   ", "   ", "   ", "   "],
            [" TR", "   ", "   ", "   ", "   ", "   "],
            [" LG", "   ", "   ", "   ", "   ", "   "],
            ["   ", "   ", "   ", "   ", "   ", "   "],
            ["   ", "   ", " BL", "XHB", " HB", "   "]]
        
        assert board_test_move == board_end or board_test_move == board_end2
    
    def test_move_quitting_traps(self):
        """ Bouger sur un traps puis rebouger case vide alors normalement life lost == 2 """
        board_test_move = [
    ["   ", "   ", "   ", "   ", "   ", "   "],
    [" CV", "   ", "   ", "   ", "   ", "   "],
    [" TR", "   ", "   ", "   ", "   ", "   "],
    [" LG", "   ", "   ", "   ", "   ", "   "],
    ["   ", "   ", "   ", "   ", "   ", "   "],
    ["X  ", "   ", " BL", " ME", " HB", "   "]]

        traps_test = [( " ME" ,0),( " GC" ,1)]
        game_state_test = (board_test_move,traps_test, game_state[2],game_state[3], game_state[4], game_state[5])

        assert lmdc.move(game_state_test, "red", 5,3) == 1
        assert lmdc.move(game_state_test, "blue", 3,3) == 1









class Test_is_game_over:
    
    #def is_game_over(board, shrimps):
    def test_game_over_continue(self):
         """La game continue, sortie = ' '. """
         board_test = [
    [" CV", "   ", "   ", "   ", "   ", "   "],
    ["   ", " BE", " CV", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " CV"],
    [" LG", "   ", "   ", "   ", " CV", "   "],
    ["   ", "   ", "   ", " CT", " BV", "   "],
    ["X  ", "   ", "   ", " CV", "   ", " CV"]
]
         assert lmdc.is_game_over(board_test, 4) == " "

    def test_game_over_win(self):
        """Gagne car 8 CV"""
        board_test = [
    [" CV", " CV", "   ", "   ", "   ", " CV"],
    ["   ", " BE", " CV", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " CV"],
    [" LG", "   ", "   ", "   ", " CV", "   "],
    ["   ", "   ", "   ", " CT", " BV", "   "],
    ["X  ", "   ", "   ", " CV", "   ", " CV"]
]
        assert lmdc.is_game_over(board_test, 4) == "Gagné"
        
    
    
    def test_game_over_nolife(self):
        """ Plus de vie ou vie négatif = La game est perdu"""
        board_end = [
            ["   ", "   ", "   ", "   ", "   ", "   "],
            [" CV", "   ", "   ", "   ", "   ", "   "],
            [" TR", "   ", "   ", "   ", "   ", "   "],
            [" LG", "   ", "   ", "   ", "   ", "   "],
            ["   ", "   ", "   ", "   ", "   ", "   "],
            ["   ", "   ", " BL", "XTB", " HB", "   "]]
         
        assert lmdc.is_game_over(board_end, 0) == "Perdu"
        assert lmdc.is_game_over(board_end, -5) == "Perdu"
        
    def test_game_over_too_much_vilains(self):
        """Test si perdu avec trop de vilains"""
        board_end = [
    [" HB", " HB", "   ", "   ", "   ", " HB"],
    ["   ", " BE", " HB", "   ", "   ", "   "],
    [" TR", "   ", " BM", " PN", "   ", " TB"],
    [" LG", "   ", "   ", "   ", " TB", "   "],
    ["   ", " HB", " HB", " CT", " BV", "   "],
    ["X  ", " HB", " HB", " HB", "   ", " TB"]
]
        assert lmdc.is_game_over(board_end, 3) == "Perdu"
        

    # Gagné lorsque board a 8Crabe Vert 
        # Perdu lorsque board = 12 vilain bleu
        # Perdu vie = 0
        # Perdu vie -1 

